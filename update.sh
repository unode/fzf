#!/usr/bin/env bash

mkdir tmp
wget -q https://api.github.com/repos/junegunn/fzf-bin/releases/latest -O - \
| grep "browser_download_url.*linux_amd64" \
| cut -d : -f 2,3 \
| tr -d \" \
| wget -qi - -O tmp/fzf-new.tgz
cd tmp || exit 1
tar xf fzf-new.tgz
VERSION="$(./fzf --version | cut -d ' ' -f 1)"
cd .. || exit 1
mv tmp/fzf "fzf-${VERSION}"
ln -sf "fzf-${VERSION}" fzf

rm -f tmp/fzf-new.tgz
rmdir tmp
